package vermeg.str;

import java.util.concurrent.atomic.AtomicLong;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import lombok.AllArgsConstructor;
import lombok.Value;

@AllArgsConstructor
@RestController
public class controlle {
     private static final String template = "Hello from %s!";
    private final AtomicLong counter = new AtomicLong();

    /**
     *
     */

    private String env ="tnlt ghj";

    @GetMapping("/")
    public Greeting greeting() {
        return new Greeting(counter.incrementAndGet(), String.format(template, env));
    }

    static public class Greeting {
        public long id;
        public String content;

        public Greeting(long id, String content) {
            this.id = id;
            this.content = content;
        }
    }
}
